const animal = "dog";

for (const letter of animal) {
    console.log(letter);
}

const persons = {
    name : "Matti", 
    age:30
};

for (const key in persons) {
    const value = persons[key];
    console.log(key);
    console.log(value);

}