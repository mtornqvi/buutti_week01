/**Exercise 8: Right-angled
Create a program that takes in a number n and
prints out a triangle of &’s with the height of n
 * Level 2:
   &
  &&&
 &&&&&
&&&&&&&
 */

const printTree = (level) => {
    let characters = 1;

    for (let i = 0; i < level; i++) {
        for (let j=0;j < level-i; j++) {
            process.stdout.write(" ");
        }
        for (let j=0; j < characters; j++) {
            process.stdout.write("&");
        }
        characters +=2;
        console.log("");
    }
};

printTree(7);
console.log("");
printTree(4);



