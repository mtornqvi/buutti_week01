const arr = ["banaani","omena","mandariini","appelsiini","kurkku","tomatti","peruna"];

console.log("Simple printout:");
arr.forEach( (alkio) => console.log(alkio));

console.log("\nContaining letter r:");
arr.forEach( (alkio) => {

    if(alkio.includes("r")) {
        console.log(alkio);
    }

}
);
// Opittavaa
const rElements = arr.filter(elem => elem.includes("r"));
console.log(rElements);

const rElementsCallBack = (elem) => {
    return elem !== "omena";
};
const rElements2 = arr.filter(rElementsCallBack);
console.log(rElements2);


const newArray = Array.from(arr);
const spreadArray = [...arr].sort();

console.log("\nSpread array:");
console.log(spreadArray);




console.log("\nSorted array:");
const arrSorted = Array.from(arr);
arrSorted.sort();
arrSorted.forEach( (alkio) => console.log(alkio));

console.log("\nModified array:");
const index = arr.indexOf("banaani");
const arrModified = Array.from(arr);
arrModified.splice(index,1);
console.log(arrModified);

const spreadArray2 = ["koira", ...arr, "sipuli"];
console.log("\nElementin lisäys:");
console.log(spreadArray2);