const arr = [1,4,6,32,25,16,31,15,10,2,7];
const max = [Number.MIN_SAFE_INTEGER,Number.MIN_SAFE_INTEGER];
for (const val of arr) {
    if (val > max[0]) {
        max[1] = max[0]; // move previous largest value to 2nd largest value
        max[0] = val;
    }
    else if (val > max[1]) {
        max[1] = val;
    }
}

console.log(`Maximum values are ${max[0]} and ${max[1]}.`);
