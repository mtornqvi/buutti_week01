const someArray = [2,3,4,5];
console.log(someArray);

const taulu = ["koira","kissa"];

// console.log(taulu[0]);

taulu[2] = "simpanssi";

// console.log(taulu[2]);

for(let i=0;i< taulu.length;i++) {
    console.log(taulu[i]);
}

// parempi laittaa uusi
const muokattuTaulu = taulu.map(alkio => alkio + " elukka ");

muokattuTaulu.forEach(alkio) => {
    console.log(alkio);
}

