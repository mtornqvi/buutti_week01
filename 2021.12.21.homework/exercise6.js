const n = process.argv[2];

let sum = 0;
for (let i=1; i<=n; i++) {
    if (i % 3 === 0 || i % 5 === 0) {
        sum += i;
    }
}
console.log(`Counting with for : Sum from 1 to ${n} with only multiples of three and fives : ${sum}.`);

