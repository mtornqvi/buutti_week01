const n = process.argv[2];

let sum = 0;
for (let i=1; i<=n; i++) {
    sum += i;
}
console.log(`Counting with for : Sum from 1 to ${n} is : ${sum}.`);

sum = 0;
let i = 1;
while (i<=n) {
    sum += i;
    i++;
}

console.log(`Counting with while : Sum from 1 to ${n} is : ${sum}.`);