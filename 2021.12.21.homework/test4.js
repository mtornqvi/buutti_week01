const recursiveLoop = (n) => {
    if (n === 5) {
        console.log("Done!");
        return n;
    }
    recursiveLoop(n + 1);
};

recursiveLoop(1);