console.log("First:");
for (let i=0; i <= 1000; i = i + 100) {
    console.log(i);
}

console.log("\nSecond:");
for (let i=1; i <= 128; i = i *2) {
    console.log(i);
}

console.log("\nThird:");
for (let i=3; i <= 15; i = i + 3) {
    console.log(i);
}

console.log("\nFourth:");
for (let i=9; i >= 0; i--) {
    console.log(i);
}

console.log("\nFifth:");
for (let i=1; i <= 4; i++) {
    for (let j=0; j<4;j++) {
        console.log(i);
    }
}

console.log("\nSixth:");
for (let i=1; i <= 3; i++) {
    for (let j=0; j<=4;j++) {
        let str  = j.toString() + " ";
        process.stdout.write(str);
    }
}