const price = 150;
const discount = 10; // in percentage

let discounted_price = (100-discount)/100*price;
console.log(`Discounted price : ${discounted_price}`)

const distance = 20; // in kilometers
const speed = 80; // km/h

let travel_time = distance/speed*60;
console.log(`Travel time (in minutes) : ${travel_time}`);

const days = 365;
const hours = days*24;
const seconds = hours*3600;

console.log(`Seconds in a year : ${seconds}`);



