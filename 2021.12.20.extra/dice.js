// Test amount of command line arguments
const n = process.argv.length;
if (n !== 3) {
  console.error(`Incorrect amount of arguments : ${n-2}. Correct usage node dice DN where N is an integer between 3 and 100.`);
  process.exit(0);
}
// Test if command line argument is preceded with "D"
const diceStr = process.argv[2];
if (diceStr[0] !== "D") {
  console.error("Correct usage node dice DN where N is an integer between 3 and 100.");
  process.exit(0);
}  
// Test if command line argument is followed by an integer
let diceSize = diceStr.substring(1);
diceSize = Number(diceSize);
if (!Number.isInteger(diceSize)) {
  console.error("Correct usage node dice DN where N is an integer between 3 and 100.");
  process.exit(0);
}  
else if (diceSize < 3 || diceSize > 100) {
  console.error("The number of sides should be a number between 3 and 100.");
  process.exit(0);
}

let readline = require("readline");
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let recursiveAsyncReadLine = function () {
  let diceResult = 0;
  rl.question("Press <enter> for a dice roll, type 'q' + <enter> to exit.\n"
    , function (line) {

      switch (line){
      case "":
        // results should be an integer between {1,diceSize}
        diceResult = 1+Math.floor(Math.random()*diceSize);

        switch(diceResult) {
        case 1:
          console.log(`Ouch! The roll is ${diceResult}.`);
          break;
        case diceSize:
          console.log(`Yippii!!! The roll is ${diceResult}.`);
          break;
        default:
          console.log(`The roll is ${diceResult}.`);
          break;
        }

        break;
      case "q":
        console.log("Bye!");
        process.exit(1);
      default:
        console.log("No such option. Please enter another: ");
      }
      recursiveAsyncReadLine(); //Calling this function again to ask new question
    });
};

recursiveAsyncReadLine();