/**  Replace characters (difficult)
 * Create a program that takes in a string, and replaces every occurrence of your given character with
your other given character.
*/

const argCount = process.argv.length;

if ( argCount !== 5) {
  console.error ("Please provide three arguments. Example usage : node exercise10 g h \"I have great grades for my grading\". Please use quotation marks");
  process.exit(0);
}

const charA = process.argv[2];
const charB = process.argv[3];


if (charA.length !==1 || charB.length !== 1) {
  console.error ("Please provide three arguments. The first two arguments should be single characters. Example usage : node exercise10 g h \"I have great grades for my grading\". Please use quotation marks");
  process.exit(0);  
}

const string = process.argv[4];

const regexp = new RegExp(charA, "gi"); // global, ignore case
let modifiedString = string.replace(regexp, charB);

console.log(modifiedString);

