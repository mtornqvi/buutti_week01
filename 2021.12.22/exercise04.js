const argCount = process.argv.length;

if ( argCount !== 3 ) {
  console.error ("Please provide only one argument. Usage : node exercise04 5");
  process.exit(0);
}

const month = Number(process.argv[2]);
if (isNaN(month)) {
  console.error ("Please provide numerical argument. Usage : node exercise04 5");
  process.exit(0);
}

function daysInMonth (month) {
  return new Date(2021, month, 0).getDate();
}

console.log(`In year 2021, in month ${month} there are : ${daysInMonth(month)} days.`);
