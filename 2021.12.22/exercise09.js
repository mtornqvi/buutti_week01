/**  Annoying substring
 * Create a program that takes in a string and drops off the last word of any given string, and
console.logs it out
*/

const argCount = process.argv.length;

if ( argCount !== 3 ) {
  console.error ("Please provide one argument. Example usage : node exercise09 lower \"Hey I'm alive!\". Please use quotation marks");
  process.exit(0);
}


const strings = process.argv[2].split(" "); 

// output all but last word
for (let i=0; i<strings.length-1;i++) {
  process.stdout.write(strings[i] + " ");
}
