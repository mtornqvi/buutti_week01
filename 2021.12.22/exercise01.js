const argCount = process.argv.length;

if (argCount !== 3) {
  console.error ("Please provide only one argument. Example usage : node exercise01 fi");
  process.exit(0);
}

const reqLang = process.argv[2].toLowerCase();
const supportedLangs = ["fi","en","zh"];

if (!supportedLangs.includes(reqLang)) {
  console.error (`Requested language code "${reqLang}" is not supported. Defaulting to English .`);
}

if (reqLang === "fi") {
  console.log("Hei maailma!");
} else if (reqLang === "en") {
  console.log("Hello world!");
} else if (reqLang === "zh") {
  console.log("你们好，世界全民！");
} else {
  console.log("Hello world!");  
}

