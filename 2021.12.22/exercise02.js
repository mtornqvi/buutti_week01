const argCount = process.argv.length;

if (argCount !== 4) {
  console.error ("Please provide exactly two arguments. Usage : node exercise02 5 2 ");
  process.exit(0);
}

const numA = Number(process.argv[2]);
const numB = Number(process.argv[3]);

if (isNaN(numA) || isNaN(numB)) {
  console.error ("Please provide numerical arguments. Usage : node exercise02 5 2 ");
  process.exit(0);   
}

if (numA > numB) {
  console.log(`${numA} is greater`);
} else if (numB > numA) {
  console.log(`${numB} is greater`);
} else {
  console.log("They are equal.");  
}
