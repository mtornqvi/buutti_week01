const argCount = process.argv.length;

if ( (argCount !== 4)  && (argCount !== 5) ) {
  console.error ("Please provide two arguments. Usage : node exercise02b 5 2 ");
  process.exit(0);
}

const numA = Number(process.argv[2]);
const numB = Number(process.argv[3]);
const passCode = process.argv[4];

if (isNaN(numA) || isNaN(numB)) {
  console.error ("Please provide numerical arguments. Usage : node exercise02 5 2 ");
  process.exit(0);   
}

if (numA > numB) {
  console.log(`${numA} is greater`);
} else if (numB > numA) {
  console.log(`${numB} is greater`);

// passcode must be passed with quotes
} else if (numA === numB && passCode === "hello world") {
  console.log("yay, you guessed the password");   
}
else {
  console.log("They are equal.");  
}
