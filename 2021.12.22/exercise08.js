/**  Modify case
 * Create a program that takes in a string, and modifies the every letter of that string to upper case or
lower case, depending on the input
*/

const argCount = process.argv.length;

if ( argCount !== 4 ) {
  console.error ("Please provide two arguments. Example usage : node exercise08 lower \"Do you LIKE Snowmen?\". Please use quotation marks");
  process.exit(0);
}

const mode = process.argv[2].toLowerCase();
const output = process.argv[3].replace(/[^a-zA-Z" "]/gm,""); // remove non-ascii or empty space from string

if (mode == "lower") {
  console.log(output.toLowerCase());
} else if (mode == "upper") {
  console.log(output.toUpperCase()); 
} else {
  console.log ("Unknown mode. Example usage : node exercise08 lower \"Do you LIKE Snowmen?\". Please use quotation marks");
}