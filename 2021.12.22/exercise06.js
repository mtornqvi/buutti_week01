/**  Create a program that takes in 3 names and outputs only initial letters of those name separated with
dot.
*/

const argCount = process.argv.length;

if ( argCount !== 5 ) {
  console.error ("Please provide three arguments. Usage : node exercise06 Jack Jack Mike");
  process.exit(0);
}

const names = process.argv.slice(2);
let initials ="";

for (const person of names) {
  const initial = person[0].toLowerCase();
  initials = initials + initial + ".";
}

// remove last dot
initials = initials.substring(0,initials.length-1);
console.log(initials);

