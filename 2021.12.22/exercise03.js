const argCount = process.argv.length;

if ( argCount !== 5 ) {
  console.error ("Please provide three arguments. Usage : node exercise03 5 2 7");
  process.exit(0);
}

const nums = [Number(process.argv[2]),Number(process.argv[3]),Number(process.argv[4])];

if ( nums.includes(NaN) ) {
  console.error ("Please provide only numerical arguments.");
  process.exit(0);
}

const numsSorted = [...nums].sort();

if (numsSorted[0] == numsSorted[1] && numsSorted[1] == numsSorted[2]) {
  console.log("All numbers are equal.");
} else {
  console.log(`Largest number is ${numsSorted[2]}.`);
  console.log(`Smallest number is ${numsSorted[0]}.`);
}

