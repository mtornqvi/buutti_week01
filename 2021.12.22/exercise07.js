/**  String length comparison
 * Create a program that takes in 3 names, and compares the length of those names. Print out the
names ordered so that the longest name is first.
*/

const argCount = process.argv.length;

if ( argCount !== 5 ) {
  console.error ("Please provide three arguments. Usage : node exercise06 Jack Jack Mike");
  process.exit(0);
}

const names = process.argv.slice(2);

let persons = [];
for (let personName of names) {
  persons.push([personName,personName.length]);
}

persons.sort(function(a,b) {
  return b[1] - a[1]; // sort according to 2nd column (reverse order)
});

for (let i=0; i<persons.length; i++) {
  process.stdout.write(persons[i][0] + " ");
}

