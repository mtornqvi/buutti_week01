let readline = require("readline");
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let recursiveAsyncReadLine = function () {
  let balance = -2;
  let isActive = true;

  rl.question("Do you want to check your balance?. Please input y/n \n"
    , function (line) {

      switch (line){
      case "y":
        
        if (isActive && balance > 0) {
          console.log(`Your balance is : ${balance}.`);
        } else if (!isActive) {
          console.log("Your account is not active.");
        } else if (balance === 0) {
          console.log("Your account is empty.");
        } else {
          console.log("Your balance is negative.");
        }

        break;
      case "n":
        console.log("Have a nice day!");
        process.exit(1);
      default:
        console.log("An erorr occurred!");
        process.exit(0);
      }
      recursiveAsyncReadLine(); //Calling this function again to ask new question
    });
};

recursiveAsyncReadLine();